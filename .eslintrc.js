const path = require('path');

module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react'],
  plugins: ['react', 'jsx-a11y', 'import'],
  parserOptions: {
    ecmaVersion: 2016,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  env: {
    node: true,
    browser: true,
    es6: true,
    jest: true
  },
  rules: {
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: ['**/*.test.*', '**/*.js']
      }
    ],
    'import/extensions': [
      'error',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
        json: 'always',
        css: 'always',
        scss: 'always'
      }
    ],
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
        'newlines-between': 'always'
      }
    ],
    'linebreak-style': 'off'
  },
  overrides: [
    {
      files: ['**/*.ts?(x)'],
      parser: 'typescript-eslint-parser',
      rules: {
        'no-undef': 'off', // https://github.com/eslint/typescript-eslint-parser/issues/77 Using Typescript's checker instead for now
        'no-unused-vars': 'off', // https://github.com/eslint/typescript-eslint-parser/issues/77 Using Typescript's checker instead for now
        'no-multi-str': 'off', // https://github.com/eslint/typescript-eslint-parser/issues/77
        'react/jsx-filename-extension': 'off', // Not working with .tsx files
        'react/prop-types': 'off', // Disabled in favor of Typescript's typechecking
        'no-unused-prop-types': 'off' // Not working with Typescript, too many false-positives anyway based on experience
      }
    },
    {
      files: ['**/*.js?(x)'],
      parser: 'babel-eslint',
      plugins: ['flowtype', 'react', 'jsx-a11y', 'import']
    }
  ],
  settings: {
    'import/resolver': {
      node: true,
      [path.resolve('./scripts/eslint-import-resolver.js')]: true
    },
    'import/parsers': {
      'typescript-eslint-parser': ['.js', '.jsx', '.ts', '.tsx']
    }
  }
};
