# APP-TEMPLATE-UI

Template for creating React web app using Javascript/Typescript

## Setup

* Install [yarn](https://www.npmjs.com/package/yarn) globally
* Install app dependencies with `yarn` command

###

## Contributing

We use [gitmoji](https://gitmoji.carloscuesta.me/) for our commit message.

### Format

```
<gitmoji> <PBI Number (optional)> <commit message>
```

Example

```
:white_check_mark: KMY-203 Adding tests
```

Output

✅ KMY-203 Adding tests

## TO DO

* Axios integration
* Redux integration
* Routing

## Migrating from Flow

(See app/components/common/Alert for example of running a flow-based file)

To easily migrate from Flow code-base, this project allows Flow annotated codes with the following work-around:

### Dev Dependencies

Added the following dev dependencies:

* babel/preset-flow
* babel-eslint
* eslint-plugin-flowtype

### Prettier

Added override for js/jsx files to use `flow` parser

```js
// .prettierrc
...
"overrides": [
  ...
  {
    "files": "*.(js|jsx)",
    "options": {
      "parser": "flow"
    }
  }
]
...
```

### ESLint

Added override for js/jsx files to use `babel-eslint` parser and include `flowtype` plugin

```js
// .eslintrc.js
module.exports = {
  //...
  overrides: [
    //...
    {
      files: ['**/*.js?(x)'],
      parser: 'babel-eslint',
      plugins: ['flowtype', 'react', 'jsx-a11y', 'import']
    }
  ]
  //...
};
```

### Webpack

Introduced a new rule exclusive to js/jsx files. `babel-loader` options was added.

```js
module: {
  rules: [
    // ...
    {
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      use: [
        { loader: 'cache-loader' },
        { loader: 'thread-loader', options: { workers: os.cpus().length - 1 } },
        {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            babelrc: false,
            presets: [
              [
                '@babel/preset-env',
                {
                  targets: {
                    browsers: 'last 2 versions'
                  },
                  loose: true,
                  modules: false
                }
              ],
              '@babel/stage-3',
              '@babel/react',
              '@babel/flow'
            ],
            plugins: [
              [
                'module-resolver',
                {
                  extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
                  root: ['./']
                }
              ]
            ]
          }
        }
      ]
    }
  ];
}
```

Ignored some typescript diagnostic codes.

```js
{
  plugins: [
    new CopyWebpackPlugin([{ from: 'assets', to: 'assets' }, { from: 'app/index.html' }]),
    new ForkTsCheckerWebpackPlugin({
      checkSyntacticErrors: true,
      watch: ['./app'],
      ignoreDiagnostics: [
        // 'type aliases' can only be used in a .ts file.
        8008,
        // 'types' can only be used in a .ts file.
        8010
      ]
    })
  ];
}
```
