/* eslint-disable no-console */

const express = require('express');
const bodyParser = require('body-parser');
const pause = require('connect-pause');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.post('/login', pause(1000), (req, res) => {
  res.send({ success: true, token: 'yoloXswag' });
});

// Start listening
app.listen(3000, () => {
  console.log('listening on port 3000');
});
