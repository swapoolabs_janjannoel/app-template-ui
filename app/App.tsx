import React from 'react';
import { render } from 'react-dom';

import AppTemplate from 'app/components/AppTemplate';

import 'app/App.styles';

const renderApp = () => {
  render(<AppTemplate />, document.getElementById('app'));
};

renderApp();

if ((module as any).hot) {
  (module as any).hot.accept('./components/AppTemplate', () => {
    renderApp();
  });
}
