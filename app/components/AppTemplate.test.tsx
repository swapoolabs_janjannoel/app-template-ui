import React from 'react';
import { mount } from 'enzyme';

import AppTemplate from './AppTemplate';

describe('AppTemplate', () => {
  test('should render correctly', () => {
    const component = mount(<AppTemplate />);
    expect(component).toMatchSnapshot();
  });
});
