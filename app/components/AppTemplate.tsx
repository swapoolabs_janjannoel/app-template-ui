import React, { StatelessComponent } from 'react';
import { Provider } from 'react-redux';

import store from 'app/store/store';
import Header from 'app/components/Layout/Header/Header';
import LandingPage from 'app/components/Landing/LandingPage';

import './AppTemplate.scss';

const AppTemplate: StatelessComponent = () => (
  <Provider store={store}>
    <>
      <Header />
      <LandingPage />
    </>
  </Provider>
);

export default AppTemplate;
