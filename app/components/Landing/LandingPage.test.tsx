import React from 'react';
import { mount } from 'enzyme';

import LandingPage from './LandingPage';

describe('LandingPage', () => {
  test('should render correctly', () => {
    const component = mount(<LandingPage />);
    expect(component).toMatchSnapshot();
  });
});
