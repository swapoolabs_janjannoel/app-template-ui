import React, { StatelessComponent } from 'react';

import './LandingPage.scss';

const LandingPage: StatelessComponent = () => <div className="page-landing">Hello World</div>;

export default LandingPage;
