import React from 'react';
import { mount } from 'enzyme';

import Header from './Header';

describe('Header', () => {
  test('should render correctly', () => {
    const component = mount(<Header />);
    expect(component).toMatchSnapshot();
  });
});
