// @flow

import React from 'react';
import classnames from 'classnames';

type Props = {
  content: string;
  context: 'error';
  className?: string;
};

const Alert = ({ content, context, className }: Props) => (
  <div
    className={classnames(`component-alert alert-${context}`, className || 'col s12', {
      'ease-in custom-show': content,
      'custom-hide': !content
    })}
  >
    <div className="alert-content-wrapper">{content}</div>
  </div>
);

Alert.defaultProps = {
  className: ''
};

export default Alert;
