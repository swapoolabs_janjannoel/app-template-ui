// @flow

import React from 'react';
import { shallow } from 'enzyme';

import Alert from './Alert';

test('Alert renders', () => {
  const error = 'error';
  const component = shallow(<Alert content={error} context="error" />);
  expect(component).toMatchSnapshot();
});
