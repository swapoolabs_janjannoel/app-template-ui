/*
 * Declare third-party libraries without type definitions here
 *
 * Example:
 *
 * declare module 'module-name' {
 *   export = any;
 * }
 *
 */

// Declare globals
declare const API_URL: string;
declare const UPLOADER_URL: string;

//  Define extensions allowed
declare module '*.json' {
  export = any;
}
