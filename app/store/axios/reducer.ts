import { Reducer } from 'redux';
import Axios, { AxiosInstance } from 'axios';

import { SessionAction, SessionActionTypes } from 'app/store/session/actions';

export type AxiosState = {
  defaultAxios: AxiosInstance;
  uploaderAxios: AxiosInstance;
};

export const DEFAULT_AXIOS_STATE = {
  defaultAxios: Axios.create({ baseURL: API_URL }),
  uploaderAxios: Axios.create({ baseURL: UPLOADER_URL })
};

const axiosReducer: Reducer<AxiosState> = (state = DEFAULT_AXIOS_STATE, action: SessionAction) => {
  switch (action.type) {
    case SessionActionTypes.LOGIN_SUCCESS:
      return {
        defaultAxios: Axios.create({
          baseURL: API_URL,
          headers: { token: action.payload.token }
        }),
        uploaderAxios: Axios.create({
          baseURL: API_URL,
          headers: { token: action.payload.token }
        })
      };
    case SessionActionTypes.LOGIN_FAIL:
      return DEFAULT_AXIOS_STATE;
    case SessionActionTypes.LOGOUT:
      return DEFAULT_AXIOS_STATE;
    default:
      return state;
  }
};

export default axiosReducer;
