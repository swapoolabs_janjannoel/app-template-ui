import { AppState } from 'app/store/reducer';

export const getDefaultAxios = (state: AppState) => state.axios.defaultAxios;

export const getUploaderAxios = (state: AppState) => state.axios.uploaderAxios;
