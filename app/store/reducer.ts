import { combineReducers } from 'redux';

import axios, { AxiosState } from 'app/store/axios/reducer';
import session, { SessionState } from 'app/store/session/reducer';

export interface AppState {
  axios: AxiosState;
  session: SessionState;
}

export default combineReducers<AppState>({ axios, session });
