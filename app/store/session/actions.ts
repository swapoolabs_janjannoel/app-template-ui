import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';

import { AppState } from 'app/store/reducer';
import { getDefaultAxios } from 'app/store/axios/selectors';

// Action Types
export enum SessionActionTypes {
  LOGIN = '@@session/LOGIN',
  LOGIN_SUCCESS = '@@session/LOGIN:SUCCESS',
  LOGIN_FAIL = '@@session/LOGIN:FAIL',
  LOGOUT = '@@session/LOGOUT',
  LOGOUT_SUCCESS = '@@session/LOGOUT:SUCCESS',
  LOGOUT_FAIL = '@@session/LOGOUT:FAIL'
}

// Actions
interface LoginAction extends Action {
  type: SessionActionTypes.LOGIN;
  payload: {
    user: string;
    password: string;
  };
}

interface LoginSuccessAction extends Action {
  type: SessionActionTypes.LOGIN_SUCCESS;
  payload: {
    token: string;
  };
}

interface LoginFailAction extends Action {
  type: SessionActionTypes.LOGIN_FAIL;
  payload: any;
}

interface LogoutAction extends Action {
  type: SessionActionTypes.LOGOUT;
}

interface LogoutSuccessAction extends Action {
  type: SessionActionTypes.LOGOUT_SUCCESS;
  payload: {
    token: string;
  };
}

interface LogoutFailAction extends Action {
  type: SessionActionTypes.LOGOUT_FAIL;
  payload: any;
}

export type SessionAction =
  | LoginAction
  | LoginSuccessAction
  | LoginFailAction
  | LogoutAction
  | LogoutSuccessAction
  | LogoutFailAction;

// Action Creators
export const loginStart = (user: string, password: string): LoginAction => ({
  type: SessionActionTypes.LOGIN,
  payload: { user, password }
});

export const loginSuccess = (token: string): LoginSuccessAction => ({
  type: SessionActionTypes.LOGIN_SUCCESS,
  payload: { token }
});

export const loginFail = (error: any): LoginFailAction => ({
  type: SessionActionTypes.LOGIN_FAIL,
  payload: { error }
});

export function login(user: string, password: string): ThunkAction<Promise<SessionAction>, AppState, void> {
  return async (dispatch, getState) => {
    dispatch(loginStart(user, password));
    const axios = getDefaultAxios(getState());
    try {
      const { data } = await axios.post('/login', { user, password });
      return dispatch(loginSuccess(data.token));
    } catch (error) {
      throw dispatch(loginFail(error));
    }
  };
}
