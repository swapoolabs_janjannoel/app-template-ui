import { Reducer } from 'redux';

import { SessionAction, SessionActionTypes } from 'app/store/session/actions';

export type SessionState = {
  lastActive?: number;
  token?: string;
  ttl: number;
};

export const DEFAULT_SESSION_STATE: SessionState = {
  lastActive: undefined,
  token: undefined,
  ttl: 0
};

const sessionReducer: Reducer<SessionState> = (state = DEFAULT_SESSION_STATE, action: SessionAction) => {
  switch (action.type) {
    case SessionActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload.token
      };
    case SessionActionTypes.LOGIN_FAIL:
      return DEFAULT_SESSION_STATE;
    case SessionActionTypes.LOGOUT:
      return DEFAULT_SESSION_STATE;
    default:
      return state;
  }
};

export default sessionReducer;
