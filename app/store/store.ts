/* eslint-disable no-underscore-dangle */
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';

import reducer, { AppState } from 'app/store/reducer';
import * as session from 'app/store/session/actions';

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  ? (window as any).window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      actionCreators: {
        ...session
      },
      actionsBlacklist: []
    })
  : compose;

const store = createStore<AppState>(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
