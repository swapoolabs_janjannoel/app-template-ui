module.exports = {
  setupFiles: ['<rootDir>/scripts/jest-setup.js'],
  moduleFileExtensions: ['js', 'jsx', 'ts', 'tsx'],
  moduleNameMapper: {
    '^.+\\.(css|scss)$': '<rootDir>/scripts/SCSSStub.js'
  },
  roots: ['<rootDir>/app'],
  testMatch: ['**/__tests__/**/*.(js|jsx|ts|tsx)', '**/?(*.)(spec|test).(js|jsx|ts|tsx)'],
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': 'babel-jest'
  },
  collectCoverageFrom: [
    '<rootDir>/app/**/*.(js|jsx|ts|tsx)',
    '!**/App.tsx',
    '!**/App.styles.ts',
    '!**/app/models/**',
    '!**/*.d.ts'
  ],
  coverageReporters: ['cobertura', 'json', 'lcov', 'text'],
  snapshotSerializers: ['enzyme-to-json/serializer']
};
