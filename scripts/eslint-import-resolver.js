const path = require('path');

const resolve = require('resolve');

module.exports = {
  interfaceVersion: 2,
  resolve(source, file, config) {
    if (resolve.isCore(source)) {
      return { found: true, path: null };
    }

    try {
      let newConfig;
      let newSource;

      if (source.indexOf('app/') === 0) {
        newSource = source.replace('app/', './');
        newConfig = Object.assign({ extensions: ['.js', '.jsx', '.ts', '.tsx', '.d.ts'] }, config, {
          basedir: path.resolve('./app')
        });
      } else {
        newSource = source;
        newConfig = Object.assign({ extensions: ['.js', '.jsx', '.ts', '.tsx', '.d.ts'] }, config, {
          basedir: path.dirname(path.resolve(file))
        });
      }

      return { found: true, path: resolve.sync(newSource, newConfig) };
    } catch (err) {
      return { found: false };
    }
  }
};
