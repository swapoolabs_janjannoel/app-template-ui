const os = require('os');

module.exports = () => {
  const networkInterfaces = os.networkInterfaces();
  let ipAddress;

  Object.keys(networkInterfaces).forEach(ifname => {
    let alias = 0;

    networkInterfaces[ifname].forEach(iface => {
      if (iface.family !== 'IPv4' || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias === 0) {
        ipAddress = iface.address;
      }
      alias += 1;
    });
  });

  return ipAddress || '127.0.0.1';
};
