/* eslint-disable no-console */

const Enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');

Enzyme.configure({ adapter: new Adapter() });

// For async tests, catch all errors here so we don't have to try / catch everywhere for safety
process.on('unhandledRejection', error => {
  console.log(error);
});

// Configure globals
global.API_URL = '';
global.UPLOADER_URL = '';
