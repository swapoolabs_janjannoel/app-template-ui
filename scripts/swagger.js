const https = require('https');
const fs = require('fs');

const environment = require('../env.json');

const file = fs.createWriteStream('swagger.json');
https.get(`${environment.development.API_URL}/api/swagger.json`, response => {
  response.pipe(file);
});
