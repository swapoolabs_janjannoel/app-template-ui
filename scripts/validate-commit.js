const fs = require('fs');

const commitMessage = fs.readFileSync(process.env.GIT_PARAMS).toString();

if (!/:[^:\s]+:\s/.test(commitMessage)) {
  throw Error('You forgot Gitmoji :( !! 😞😞😞');
}
