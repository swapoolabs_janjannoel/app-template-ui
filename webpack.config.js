const path = require('path');
const os = require('os');

const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WriteFileWebpackPlugin = require('write-file-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

const getIpAddress = require('./scripts/helpers/get-ip-address');
const environment = require('./env.json');

function config(env) {
  const isDevelopment = env.mock || env.dev;
  const isProduction = env.development || env.staging;

  const CONFIG = {
    context: __dirname,
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'scripts.js',
      publicPath: '/',
      devtoolModuleFilenameTemplate: info => path.resolve(info.absoluteResourcePath).replace(/\\/g, '/')
    },
    devServer: {
      contentBase: path.join(__dirname, 'dist'),
      hot: true,
      publicPath: '/',
      historyApiFallback: true,
      host: '0.0.0.0'
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', '.jsx']
    },
    stats: {
      colors: true,
      reasons: true,
      chunks: true
    },
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          loader: 'eslint-loader'
        },
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: [
            { loader: 'cache-loader' },
            { loader: 'thread-loader', options: { workers: os.cpus().length - 1 } },
            { loader: 'babel-loader', options: { cacheDirectory: true } }
          ]
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: [
            { loader: 'cache-loader' },
            { loader: 'thread-loader', options: { workers: os.cpus().length - 1 } },
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
                babelrc: false,
                presets: [
                  [
                    '@babel/preset-env',
                    {
                      targets: {
                        browsers: 'last 2 versions'
                      },
                      loose: true,
                      modules: false
                    }
                  ],
                  '@babel/stage-3',
                  '@babel/react',
                  '@babel/flow'
                ],
                plugins: [
                  [
                    'module-resolver',
                    {
                      extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
                      root: ['./']
                    }
                  ]
                ]
              }
            }
          ]
        },
        {
          test: /\.(css|scss)$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              { loader: 'css-loader', options: { sourceMap: isDevelopment } },
              {
                loader: 'sass-loader',
                options: {
                  sourceMap: isDevelopment,
                  data: '@import "app/styles/globals";',
                  includePaths: [__dirname]
                }
              }
            ]
          })
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2)$/,
          loader: 'file-loader?name=fonts/[name].[ext]'
        }
      ]
    },
    plugins: [
      new CopyWebpackPlugin([{ from: 'assets', to: 'assets' }, { from: 'app/index.html' }]),
      new ForkTsCheckerWebpackPlugin({
        checkSyntacticErrors: true,
        watch: ['./app'],
        ignoreDiagnostics: [
          // 'type aliases' can only be used in a .ts file.
          8008,
          // 'types' can only be used in a .ts file.
          8010,
          // 'type arguments' can only be used in a .ts file.
          8011
        ]
      }),
      new StyleLintPlugin()
    ]
  };

  if (isDevelopment) {
    CONFIG.entry = [
      'babel-polyfill',
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:8080',
      'webpack/hot/only-dev-server',
      './app/App.tsx'
    ];
    CONFIG.plugins = CONFIG.plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
      new ExtractTextPlugin({ disable: true }),
      new WriteFileWebpackPlugin({
        test: /(\/assets\/|index.html)/
      })
    ]);

    if (env.mock) {
      // mock
      const ipAddress = getIpAddress();

      CONFIG.devtool = 'cheap-eval-source-map';
      CONFIG.plugins = CONFIG.plugins.concat([
        new webpack.DefinePlugin({
          API_URL: JSON.stringify(`http://${ipAddress}:3000`),
          UPLOADER_URL: JSON.stringify(`http://${ipAddress}:3000`)
        })
      ]);
    }

    if (env.dev) {
      // dev
      CONFIG.devtool = 'cheap-eval-source-map';
      CONFIG.plugins = CONFIG.plugins.concat([
        new webpack.DefinePlugin({
          API_URL: JSON.stringify('http://localhost:3000'),
          UPLOADER_URL: JSON.stringify('http://localhost:3000')
        })
      ]);
    }
  }

  if (isProduction) {
    CONFIG.entry = ['babel-polyfill', './app/App.tsx'];
    CONFIG.devtool = false;
    CONFIG.plugins = CONFIG.plugins.concat([
      new ExtractTextPlugin({ filename: 'styles.css', disable: false, allChunks: true })
    ]);

    if (env.development) {
      // development
      CONFIG.plugins = CONFIG.plugins.concat([
        new webpack.DefinePlugin({
          API_URL: JSON.stringify(environment.development.API_URL),
          UPLOADER_URL: JSON.stringify(environment.development.UPLOADER_URL)
        })
      ]);
    }

    if (env.staging) {
      // staging
      CONFIG.plugins = CONFIG.plugins.concat([
        new webpack.DefinePlugin({
          API_URL: JSON.stringify(environment.staging.API_URL),
          UPLOADER_URL: JSON.stringify(environment.staging.UPLOADER_URL)
        })
      ]);
    }
  }

  return CONFIG;
}

module.exports = config;
